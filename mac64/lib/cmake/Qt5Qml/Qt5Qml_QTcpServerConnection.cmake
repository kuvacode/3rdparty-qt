
add_library(Qt5::QTcpServerConnection MODULE IMPORTED)

_populate_Qml_plugin_properties(QTcpServerConnection RELEASE "qmltooling/libqmldbg_tcp.dylib")
_populate_Qml_plugin_properties(QTcpServerConnection DEBUG "qmltooling/libqmldbg_tcp.dylib")

list(APPEND Qt5Qml_PLUGINS Qt5::QTcpServerConnection)
