
add_library(Qt5::QtQuick2Plugin MODULE IMPORTED)

_populate_Qml_plugin_properties(QtQuick2Plugin RELEASE "qmltooling/libqmldbg_qtquick2.dylib")
_populate_Qml_plugin_properties(QtQuick2Plugin DEBUG "qmltooling/libqmldbg_qtquick2.dylib")

list(APPEND Qt5Qml_PLUGINS Qt5::QtQuick2Plugin)
